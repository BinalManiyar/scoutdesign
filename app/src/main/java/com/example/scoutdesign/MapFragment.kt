package com.example.scoutdesign

import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.activity.OnBackPressedDispatcher
import androidx.activity.addCallback
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment
import com.google.android.gms.location.places.ui.PlaceSelectionListener
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions


class MapFragment : Fragment() {

    lateinit var list: List<Address>
    lateinit var mapFragment: SupportMapFragment
    lateinit var googleMap: GoogleMap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        var view: View = inflater.inflate(R.layout.fragment_map, container, false)

        var tvLogin: TextView = view.findViewById(R.id.tvLogin)
        var etAddress: EditText = view.findViewById(R.id.etAddress)
        etAddress.isFocusableInTouchMode = true


        etAddress.setOnEditorActionListener { v, actionId, event ->

            if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                var add: String = etAddress.text.toString()


                if(add.isNotEmpty()){
                    var geocoder: Geocoder = Geocoder(context)
                    list = geocoder.getFromLocationName(add,1)
                }

                var address: Address = list.get(0)
                var latLng = LatLng(address.latitude,address.longitude)
                googleMap.addMarker(MarkerOptions().position(latLng).title("Your location"))
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,10f))

                true

            } else {
                false
            }

        }

        tvLogin.setOnClickListener {
            findNavController().navigate(R.id.action_mapFragment_to_loginFragment2)
        }

        mapFragment = childFragmentManager.findFragmentById(R.id.GoogleMap) as SupportMapFragment
        mapFragment.getMapAsync(OnMapReadyCallback {
            googleMap = it

            val location = LatLng(21.9612, 70.7939)
            googleMap.addMarker(MarkerOptions().position(location).title("My location"))
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 10f))

        })
        return view

    }

}