package com.example.scoutdesign

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController

class LoginFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        var view: View = inflater.inflate(R.layout.fragment_login, container, false)

        var tvSignUp: TextView = view.findViewById(R.id.tvSignUp)

        tvSignUp.setOnClickListener {

            var action: NavDirections =
                LoginFragmentDirections.actionLoginFragment2ToSignUpFragment()
            findNavController().navigate(action)
        }
        return view
    }


}