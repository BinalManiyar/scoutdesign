package com.example.scoutdesign

import android.os.Bundle
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import java.util.regex.Pattern


class SignUpFragment : Fragment() {

    private val PASSWORD_PATTERN: Pattern = Pattern.compile(
        "^(?=.*[0-9])" +
                "(?=.*[a-z])" +
                "(?=.*[A-Z])" +
                "(?=.*[@#$%^&+=])" +
                "(?=\\S+$)" +
                ".{6,}" +
                "$"
    )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view: View = inflater.inflate(R.layout.fragment_sign_up, container, false)


        var fName: EditText = view.findViewById(R.id.etsignUpFirstName)
        var lName: EditText = view.findViewById(R.id.etsignUpLastName)
        var email: EditText = view.findViewById(R.id.etsignUpEmailAddress)
        var phoneNo: EditText = view.findViewById(R.id.etsignUpPhoneNumber)
        var password: EditText = view.findViewById(R.id.etSignUpPassword)
        var confirmPassword: EditText = view.findViewById(R.id.etsignUpConfirmPassword)


        var tvLogin: TextView = view.findViewById(R.id.tvLogin)
        var tvSignUpNext: TextView = view.findViewById(R.id.tvSignUpNext)

        tvLogin.setOnClickListener {

            findNavController().navigate(R.id.action_signUpFragment_to_loginFragment2)

        }

        fun validateFirstName(): Boolean {
            if (fName.text.toString().isEmpty()) {
                fName.error = "Field cannot be empty!"
                return false
            } else {
                fName.error = null
                return true
            }
        }

        fun validateLastName(): Boolean {
            if (lName.text.toString().isEmpty()) {
                lName.error = "Field cannot be empty!"
                return false
            } else {
                lName.error = null
                return true
            }
        }

        fun validateEmail(): Boolean {
            if (email.text.toString().isEmpty()) {
                email.error = "Field cannot be empty!"
                return false
            } else if (!Patterns.EMAIL_ADDRESS.matcher(email.text.toString()).matches()) {
                email.error = "Invalid email address!"
                return false
            } else {
                email.error = null
                return true
            }
        }

        fun validatePhoneNo(): Boolean {
            if (phoneNo.text.toString().isEmpty()) {
                phoneNo.error = "Field cannot be empty!"
                return false
            } else if (!Patterns.PHONE.matcher(phoneNo.text.toString()).matches()) {
                phoneNo.error = "Invalid phone number"
                return false
            } else {
                phoneNo.error = null
                return true
            }
        }

        fun validatePassword(): Boolean {
            if (password.text.toString().isEmpty()) {
                password.error = "Field cannot be empty!"
                return false
            } else if (!PASSWORD_PATTERN.matcher(password.text.toString()).matches()) {
                password.error = "Password too weak"
                return false
            } else {
                password.error = null
                return true
            }
        }

        fun validateConfirmPassword(): Boolean {
            if (confirmPassword.text.toString().isEmpty()) {
                confirmPassword.error = "Field cannot be empty!"
                return false
            } else if (!confirmPassword.text.toString().equals(password.text.toString())) {
                confirmPassword.error = "Does not match with password"
                return false

            } else {
                confirmPassword.error = null
                return true
            }
        }

        tvSignUpNext.setOnClickListener {

            if (!validateFirstName() || !validateLastName() || !validateEmail() || !validatePhoneNo() || !validatePassword() || !validateConfirmPassword()) {
                return@setOnClickListener
            } else {
                findNavController().navigate(R.id.action_signUpFragment_to_signUpTwo)
            }
        }
        return view
    }
}
