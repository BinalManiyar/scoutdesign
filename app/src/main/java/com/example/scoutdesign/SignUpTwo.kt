package com.example.scoutdesign

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageButton
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.activity.addCallback
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController

class SignUpTwo : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment


        var view: View = inflater.inflate(R.layout.fragment_sign_up_two, container, false)

        var practiceName: EditText = view.findViewById(R.id.etPracticeName)
        var noOfOperatories: EditText = view.findViewById(R.id.etNumberOfOperatories)

        var tvLogin : TextView = view.findViewById(R.id.tvLogin)
        var etPracAddress: EditText = view.findViewById(R.id.etPracticeAddress)

        fun validatePracticeName(): Boolean {
            if (practiceName.text.toString().isEmpty()) {
                practiceName.error = "Field cannot be empty!"
                return false
            } else {
                practiceName.error = null
                return true
            }
        }

        fun validateOperatories(): Boolean {
            if (noOfOperatories.text.toString().isEmpty()) {
                noOfOperatories.error = "Field cannot be empty!"
                return false
            } else {
                noOfOperatories.error = null
                return true
            }
        }

        etPracAddress.setOnClickListener {

            if(!validatePracticeName() || !validateOperatories()){
                return@setOnClickListener
            }else {
                findNavController().navigate(R.id.action_signUpTwo_to_mapFragment)
            }
        }

        tvLogin.setOnClickListener {

            findNavController().navigate(R.id.action_signUpTwo_to_loginFragment2)

        }
        return view
    }
}